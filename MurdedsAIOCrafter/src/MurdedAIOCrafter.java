import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Entity;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.skill.Skills;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.utility.Area;
	
	
	@ScriptManifest(name = "Murded's AIO Crafter", author = "Murded", version = 1D, info="Verseion 1.0 - Supports Cutting Gems, Creating Armour and Creating Jewelery")
	public class MurdedAIOCrafter extends Script {
		
		final int NEEDLE = 1733;
		final int THREAD = 1734;
		
		final int LEATHER = 1741;		
		final int LEATHER_GLOVES = 1059;
		final int LEATHER_BOOTS = 1061;
		final int LEATHER_COWL = 1167;
		final int LEATHER_VAMBS = 1063;
		final int LEATHER_BODY = 1129;
		final int LEATHER_CHAPS = 1095;
		final int LEATHER_COIF = 1169;

		final int HARD_LEATHER = 1743;
		final int HARD_LEATHER_BODY = 1131;
		
		final int SNAKESKIN = 6289;
		final int SNAKESKIN_BOOTS = 6328;
		final int SNAKESKIN_BANDANA = 6326;
		final int SNAKESKIN_VAMBS = 6330;
		final int SNAKESKIN_CHAPS = 6324;
		final int SNAKESKIN_BODY = 6322;
		
		final int GREENDHIDE = 1745;
		final int GREENDHIDE_VAMBS = 1065;
		final int GREENDHIDE_CHAPS = 1099;
		final int GREENDHIDE_BODY = 1135;
		
		final int BLUEDHIDE = 2505;
		final int BLUEDHIDE_VAMBS = 2487;
		final int BLUEDHIDE_CHAPS = 2493;
		final int BLUEDHIDE_BODY = 2499;
		
		final int REDDHIDE = 2507;
		final int REDDHIDE_VAMBS = 2489;
		final int REDDHIDE_CHAPS = 2495;
		final int REDDHIDE_BODY = 2501;
		
		final int BLACKDHIDE = 2509;
		final int BLACKDHIDE_VAMBS = 2491;
		final int BLACKDHIDE_CHAPS = 2497;
		final int BLACKDHIDE_BODY = 2503;
		
		final int CHISSEL = 1755;
		
		final int U_OPAL = 1625;
		final int C_OPAL = 1609;
		
		final int U_JADE = 1627;
		final int C_JADE = 1611;
		
		final int U_TOPAZ = 1629;
		final int C_TOPAZ = 1613;
		
		final int U_SAPPHIRE = 1623;
		final int C_SAPPHIRE = 1607;
		
		final int U_EMERALD = 1621;
		final int C_EMERALD = 1605;
		
		final int U_RUBY = 1619;
		final int C_RUBY = 1603;
		
		final int U_DIAMOND = 1617;
		final int C_DIAMOND = 1601;
		
		final int U_DRAGONSTONE = 1631;
		final int C_DRAGONSTONE = 1615;
		
		final int U_ONYX = 6571;
		final int C_ONYX = 6573;
		
		final int GOLDBAR = 2357;
		final int MOULD_RING = 1592;
		final int MOULD_NECKLACE = 1597;
		final int MOULD_BRACELET = 11065;
		final int MOULD_AMULET = 1595;
		
		final int GOLD_RING = 1635;
		final int GOLD_NECKLACE = 1654;
		final int GOLD_BRACELET = 11069;
		final int GOLD_AMULET = 11069;
		
		final int SAPPHIRE_RING = 1637;
		final int SAPPHIRE_NECKLACE = 1656;
		final int SAPPHIRE_BRACELET = 11072;
		final int SAPPHIRE_AMULET = 1675;
		
		final int EMERALD_RING = 1639;
		final int EMERALD_NECKLACE = 1658;
		final int EMERALD_BRACELET = 11076;
		final int EMERALD_AMULET = 1677;
		
		final int RUBY_RING = 1641;
		final int RUBY_NECKLACE = 1660;
		final int RUBY_BRACELET = 11085;
		final int RUBY_AMULET = 1679;
		
		final int DIAMOND_RING = 1643;
		final int DIAMOND_NECKLACE = 1662;
		final int DIAMOND_BRACELET = 11092;
		final int DIAMOND_AMULET = 1681;
		
		final int DRAGONSTONE_RING = 1645;
		final int DRAGONSTONE_AMULET = 1683;
		
		final int ONYX_RING = 6575;
		final int ONYX_NECKLACE = 6577;
		final int ONYX_BRACELET = 11130;
		final int ONYX_AMULET = 6579;
		
		
		public String method = "JEWELERY2"; // ARMOUR, GEMS, JEWELERY, JEWELERY2
		public int material = 2357;
		public int gemID = 1607;
		public int craftTimer = 0;
		public int MOULD = 1592;
		
		public boolean walkingTobank = false;
		public boolean hasNeeded = false;
		
		
		public int ammountToWithdraw = 13;
		
		public int interfaceID = 446;
		public int childID = 68;
		public boolean waitGui = true;
		
		public String produce = "None";
		
		public static long startTime;
		
		public int startXp = 0;
		
		
		
		
		final Area BANK_AREA = new Area(2943,3373,2949,3368);
		final Area OUTSIDEBANKAREA = new Area(2954,3379,2948,3384);
		final Area CENTREAREA = new Area(2967,3379,2963,3384);
		final Area FURNACE_AREA = new Area(2978,3367,2968,3376);
		
		
	    final Position walkToBank = new Position(2946, 3370, 0);
	    final Position outSideBank = new Position(2951, 3381, 0);
	    final Position centre = new Position(2964, 3380, 0);
	    final Position atFurnace = new Position(2973, 3372, 0);
	    
		 MainGUI g = new MainGUI();
	    ARMOURGUI g2 = new ARMOURGUI();
	    GEMSGUI g3 = new GEMSGUI();
	    JEWELERY g4 = new JEWELERY();
	    
	    Image paint = getImage("http://img849.imageshack.us/img849/772/n0nk.png");
	  


		
		
		
		
		enum State {
			setup, createArmour, getEssentials, getMaterial, getThread, getNeedle, bankItems, cutGems, getChissel, makeRing, makeNecklace, makeBracelet, makeAmulet, getMould, createJewelery, smelting, getGem, createArmour2, createArmour3
		}
	
		private State state;
			
		public void onStart(){
			
			

			 

			g.setVisible(true);
			
			startTime = System.currentTimeMillis();
			
			startXp = client.getSkills().getExperience(Skill.CRAFTING);
				
			if(method == "ARMOUR") {
			state = State.createArmour;
			} else if(method == "GEMS") {
				state = State.cutGems;
			}
			
		}
		
		public void onExit(){

			 

			g.dispose();
			g2.dispose();
			g3.dispose();
			g4.dispose();
			
			log("Thank You For Using Murded's AIO Crafter");
			

			
		}
		
		private Image getImage(String url) {
	         try { return ImageIO.read(new URL(url)); } 
	         catch(IOException e) { return null; }
	       }
		
		public int onLoop() throws InterruptedException{
			
			while(waitGui) {
				sleep(1000);
			}
			
			if(hasNeeded) {
			walkingTobank = false;	
			} else {
				walkingTobank = true;
			}

			if(method == "ARMOUR") {
				
				if(client.getInventory().contains(THREAD) == false) {
					state = State.getThread;
					
				}else if (client.getInventory().contains(NEEDLE) == false) {
						state = State.getNeedle;
					
				} else if(client.getInventory().contains(material) == false) {
					if(client.getInventory().isFull() == false) {
						state = State.getMaterial;
					}
				}else if(client.getInventory().contains(NEEDLE)) {
					if(client.getInventory().contains(THREAD)) {
						if(client.getInventory().contains(material)) {
							if(client.getBank().isOpen() == false) {
								
							
							
						
						state = State.createArmour;
						}
					}
				}

			}

				if(client.getInventory().contains(material) == false) {
				if(client.getInventory().contains(produce)) {
					state = State.bankItems;
				}
			}
			}
			
			if(method == "ARMOUR2") {
				
				if(client.getInventory().contains(THREAD) == false) {
					state = State.getThread;
					
				}else if (client.getInventory().contains(NEEDLE) == false) {
						state = State.getNeedle;
					
				} else if(client.getInventory().contains(material) == false) {
					if(client.getInventory().isFull() == false) {
						state = State.getMaterial;
					}
				}else if(client.getInventory().contains(NEEDLE)) {
					if(client.getInventory().contains(THREAD)) {
						if(client.getInventory().contains(material)) {
							if(client.getBank().isOpen() == false) {
								
							
							
						
						state = State.createArmour2;
						}
					}
				}

			}

				if(client.getInventory().contains(material) == false) {
				if(client.getInventory().contains(produce)) {
					state = State.bankItems;
				}
			}
			}
			
			if(method == "ARMOUR3") {
				
				if(client.getInventory().contains(THREAD) == false) {
					state = State.getThread;
					
				}else if (client.getInventory().contains(NEEDLE) == false) {
						state = State.getNeedle;
					
				} else if(client.getInventory().contains(material) == false) {
					if(client.getInventory().isFull() == false) {
						state = State.getMaterial;
					}
				}else if(client.getInventory().contains(NEEDLE)) {
					if(client.getInventory().contains(THREAD)) {
						if(client.getInventory().contains(material)) {
							if(client.getBank().isOpen() == false) {
								
							
							
						
						state = State.createArmour3;
						}
					}
				}

			}

				if(client.getInventory().contains(material) == false) {
				if(client.getInventory().contains(produce)) {
					state = State.bankItems;
				}
			}
			}
			
			
			
			if(method == "GEMS") {
				if(client.getInventory().contains(CHISSEL) == false) {
					state = State.getChissel;
				
				} else if (client.getInventory().contains(material) == false) {
					if(client.getInventory().isFull() == false) {
						state = State.getMaterial;
					}
				} else if(client.getInventory().contains(CHISSEL)) {
					if(client.getInventory().contains(material)) {
						if(client.getBank().isOpen() == false) {
							
							state = State.cutGems;
						}
					}
				}
			
			
			if(client.getInventory().contains(material) == false) {
				if(client.getInventory().contains(produce)) {
					state = State.bankItems;
				}
			}
			}
			
			if(method == "JEWELERY") {
				
				
				if(BANK_AREA.contains(client.getMyPlayer())) {
					
					if(client.getInventory().isFull() == false) {
					
					if(client.getInventory().contains(MOULD) == false) {
						state = State.getMould;
					} else if(client.getInventory().contains(MOULD)) {

					}
					
					if(client.getInventory().contains(material) == false) {
						state = State.getMaterial;
					} else if(client.getInventory().contains(material)) {
						
					}
					

					} else if(client.getInventory().isFull()) {
						if(client.getInventory().contains(material) == false) {

							state = State.bankItems;
						}
						
						
					}
					

				}
					
					
				
				if(client.getInventory().contains(MOULD)) {
					if(client.getInventory().contains(material)) {
						hasNeeded = true;
					} else if(client.getInventory().contains(MOULD) == false) {
						hasNeeded = false;
					} else if(client.getInventory().contains(material) == false) {
						hasNeeded = false;
					}
				}
				
				if(hasNeeded) {
				walkingTobank = false;	
				} else {
					walkingTobank = true;
				}
				
				
				if(hasNeeded) {
					if(walkingTobank == false) {
						
						if(BANK_AREA.contains(client.getMyPlayer())) {
							walkMiniMap(outSideBank);
						}
						
						
						if(OUTSIDEBANKAREA.contains(client.getMyPlayer())) {
							walkMiniMap(centre);
						} else if(CENTREAREA.contains(client.getMyPlayer()))  {
							walkMiniMap(atFurnace);
						} else if(FURNACE_AREA.contains(client.getMyPlayer())) {
							if(state != State.smelting) {
							state = State.createJewelery;
						}
						}
						
						
					}
	
						
						
				} else if(hasNeeded == false) {
					if(walkingTobank == true) {
						if(BANK_AREA.contains(client.getMyPlayer())) {
							walkingTobank = false;
						}
						
						if(OUTSIDEBANKAREA.contains(client.getMyPlayer())) {
							walkMiniMap(walkToBank);
						} else if(CENTREAREA.contains(client.getMyPlayer()))  {
							walkMiniMap(outSideBank);
						} else if(FURNACE_AREA.contains(client.getMyPlayer())) {
							walkMiniMap(centre);
						}
						
					}
				}

					
					
				}

			
			if(method == "JEWELERY2") {
				
				
				if(BANK_AREA.contains(client.getMyPlayer())) {
					
					if(client.getInventory().isFull() == false) {
					
					if(client.getInventory().contains(MOULD) == false) {
						state = State.getMould;
					} else if(client.getInventory().contains(MOULD)) {

					}
					
					if(client.getInventory().contains(material) == false) {
						state = State.getMaterial;
					} else if(client.getInventory().contains(material)) {
						
					}
					
					if(client.getInventory().contains(gemID) == false) {
						state = State.getGem;
					}
					

					} else if(client.getInventory().isFull()) {
						if(client.getInventory().contains(material) == false) {

							state = State.bankItems;
						}
						
						
					}
					

				}
					
					
				
				if(client.getInventory().contains(MOULD)) {
					if(client.getInventory().contains(material)) {
						if(client.getInventory().contains(gemID)) {
						hasNeeded = true;
						}
					} else if(client.getInventory().contains(MOULD) == false) {
						hasNeeded = false;
					} else if(client.getInventory().contains(material) == false) {
						hasNeeded = false;
					} else if(client.getInventory().contains(gemID) == false){
						hasNeeded = false;
					}
				}

				if(hasNeeded) {
					if(walkingTobank == false) {
						
						if(BANK_AREA.contains(client.getMyPlayer())) {
							walkMiniMap(outSideBank);
						}
						
						
						if(OUTSIDEBANKAREA.contains(client.getMyPlayer())) {
							walkMiniMap(centre);
						} else if(CENTREAREA.contains(client.getMyPlayer()))  {
							walkMiniMap(atFurnace);
						} else if(FURNACE_AREA.contains(client.getMyPlayer())) {
							if(state != State.smelting) {
							state = State.createJewelery;
						}
						}
						
						
					}
	
						
						
				} else if(hasNeeded == false) {
					if(walkingTobank == true) {
						if(BANK_AREA.contains(client.getMyPlayer())) {
							walkingTobank = false;
						}
						
						if(OUTSIDEBANKAREA.contains(client.getMyPlayer())) {
							walkMiniMap(walkToBank);
						} else if(CENTREAREA.contains(client.getMyPlayer()))  {
							walkMiniMap(outSideBank);
						} else if(FURNACE_AREA.contains(client.getMyPlayer())) {
							walkMiniMap(centre);
						}
						
					}
				}

					
					
				}
			
			
			
			switch (state){
			case setup:
				return SETUP();
			case createArmour:
				return CREATEARMOUR();
			case getNeedle:
				return GETNEEDLE();
			case getThread:
				return GETTHREAD();
			case getMaterial:
				return GETMATERIAL();
			case bankItems:
				return BANKITEMS();
			case cutGems:
				return CUTGEMS();
			case getChissel:
			return GETCHISSEL();
			case getMould:
				return GETMOULD();
			case createJewelery:
				return CREATEJEWELERY();
			case smelting:
				return SMELTING();
			case getGem:
				return GETGEM();
			case createArmour2:
				return CREATEARMOUR2();
			case createArmour3:
				return CREATEARMOUR3();
				
			}
		return random(10, 20);
		}
	
		int SETUP() throws InterruptedException {
			
			return 100;
		}
		
		int SMELTING() {
			craftTimer = craftTimer + 1;
			
			if(craftTimer >= 4) {
				
				state = State.createJewelery;
				craftTimer = 0;
				}
				
				if(client.getMyPlayer().isAnimating()) {
					craftTimer = 0;
				}
			
			
			return 1000;
		}
		int GETMOULD() throws InterruptedException {
			

			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getInventory().contains(MOULD) == false) {
				if(client.getBank().isOpen() == false) {
					bank.interact("Bank");
					
				} else if (client.getBank().isOpen()) {
					client.getBank().withdraw1(MOULD);
					
				}
				
				
			}
			
			state = State.setup;
			
			return 1000 + random(400);
		}

		int CREATEARMOUR3() throws InterruptedException {
			
			if(client.getMyPlayer().isAnimating() == false) {
				craftTimer = craftTimer + 1;
		}
			
			

		


		if(craftTimer >= 1) {
		
		client.getInventory().interactWithId(CHISSEL, "Use");
		sleep(1000);
		client.getInventory().interactWithId(material, "Use");
		craftTimer = 0;
		}
		
		if(client.getMyPlayer().isAnimating()) {
			craftTimer = 0;
		}
			
			return 1000;
		}
		
		int CUTGEMS() throws InterruptedException {
			
			if(client.getMyPlayer().isAnimating() == false) {
				craftTimer = craftTimer + 1;
		}
			
			

		


		if(craftTimer >= 4) {
		
		client.getInventory().interactWithId(CHISSEL, "Use");
		sleep(1000);
		client.getInventory().interactWithId(material, "Use");
		sleep(1000);
		client.getInterface(interfaceID).getChild(childID).interact("Make X");
		sleep(1000);
		client.typeString("99");
		craftTimer = 0;
		}
		
		if(client.getMyPlayer().isAnimating()) {
			craftTimer = 0;
		}
			
			return 1000;
		}
		
		

		int GETCHISSEL() throws InterruptedException {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getInventory().contains(CHISSEL) == false) {
				if(client.getBank().isOpen() == false) {
					bank.interact("Bank");
					
				} else if (client.getBank().isOpen()) {
					client.getBank().withdraw1(CHISSEL);
					
				}
				
				
			}
			
			
			return 1000 + random(400);
		}

		
		
		int GETMATERIAL() throws InterruptedException {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			
			
			
			

			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			}
			

			if(client.getInventory().contains(material) == false) {
				client.getBank().withdrawX(material, ammountToWithdraw);
			}


			state = State.setup;
			return 1000;
		}
		
int GETGEM() throws InterruptedException {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			
			
			
			

			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			}
			

			if(client.getInventory().contains(gemID) == false) {
				client.getBank().withdrawX(gemID, ammountToWithdraw);
			}

			state = State.setup;
			return 1000;
		}
		
		int GETTHREAD() throws InterruptedException {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getInventory().contains(THREAD) == false) {
				if(client.getBank().isOpen() == false) {
					bank.interact("Bank");
					
				} else if (client.getBank().isOpen()) {
					client.getBank().withdrawX(THREAD, 25);
					
				}
				
				
			} 
			
			
			return 1000 + random(400);
		}
		
		int GETNEEDLE() throws InterruptedException {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getInventory().contains(NEEDLE) == false) {
				if(client.getBank().isOpen() == false) {
					bank.interact("Bank");
					
				} else if (client.getBank().isOpen()) {
					client.getBank().withdraw1(NEEDLE);
					
				}
				
				
			}
			
			
			return 1000 + random(400);
		}
		
		int BANKITEMS() throws InterruptedException {
			
			

			if(method == "ARMOUR") {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			} else {
					client.getBank().depositAllExcept(NEEDLE,THREAD);
					
				}
			
			} else 	if(method == "GEMS") {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			} else {
					client.getBank().depositAllExcept(CHISSEL);
					
				}
			
			} else if(method == "JEWELERY") {
			
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			} else {
					client.getBank().depositAllExcept(MOULD);
					
				}
			
			} else if(method == "JEWELERY2") {
				
			RS2Object bank = closestObjectForName("Bank booth");
			
			if(client.getBank().isOpen() == false) {
				bank.interact("Bank");
			} else {
					client.getBank().depositAllExcept(MOULD);
					
				}
			
			}

			
			
			
			return 1000;
		}

int CREATEARMOUR() throws InterruptedException {
	
	
	if(client.getMyPlayer().isAnimating() == false) {
			craftTimer = craftTimer + 1;
	}

	


	if(craftTimer >= 4) {
	
	client.getInventory().interactWithId(NEEDLE, "Use");
	sleep(1000);
	client.getInventory().interactWithId(material, "Use");
	sleep(1000);
	client.getInterface(interfaceID).getChild(childID).interact("Make All pairs of " + produce);
	craftTimer = 0;
	}
	
	if(client.getMyPlayer().isAnimating()) {
		craftTimer = 0;
	}

			
			return 1000 + random(400);
		}

int CREATEARMOUR2() throws InterruptedException {
	
	
	if(client.getMyPlayer().isAnimating() == false) {
			craftTimer = craftTimer + 1;
	}

	


	if(craftTimer >= 4) {
	
	client.getInventory().interactWithId(NEEDLE, "Use");
	sleep(1000);
	client.getInventory().interactWithId(material, "Use");
	sleep(1000);
	client.getInterface(interfaceID).getChild(childID).interact("Make X");
	sleep(1000);
	client.typeString("99");
	craftTimer = 0;
	}
	
	if(client.getMyPlayer().isAnimating()) {
		craftTimer = 0;
	}

			
			return 1000 + random(400);
		}



int CREATEJEWELERY() throws InterruptedException {
	
	Entity Furnace = closestObjectForName("Furnace");
	


	if(client.getInterface(446) == null) {

	client.getInventory().interactWithId(material, "Use");
	sleep(1000 + random(400));
	Furnace.interact("Use", "Gold bar" + " -> Furnace",
	false, 5, false, true);
	sleep(1000 + random(400));

	} else if(client.getInterface(446) != null) {
		client.getInterface(interfaceID).getChild(childID).interact("Make X");
		sleep(1000 + random(400));
		client.typeString("99");
		state = State.smelting;
		
	}




			
			return 1000 + random(400);

		}

public String runTime(long i) {
DecimalFormat nf = new DecimalFormat("00");
long millis = System.currentTimeMillis() - i;
long hours = millis / (1000 * 60 * 60);
millis -= hours * (1000 * 60 * 60);
long minutes = millis / (1000 * 60);
millis -= minutes * (1000 * 60);
long seconds = millis / 1000;
return nf.format(hours) + ":" + nf.format(minutes) + ":" + nf.format(seconds);
}

public String perHour(int gained) {
    return formatNumber((int) ((gained) * 3600000D / (System.currentTimeMillis() - startTime)));
}

public String formatNumber(int start) {
    DecimalFormat nf = new DecimalFormat("0.0");
    double i = start;
    if(i >= 1000000) {
        return nf.format((i / 1000000)) + "m";
    }
    if(i >=  1000) {
        return nf.format((i / 1000)) + "k";
    }
    return ""+start;
}



		
		

		@Override
		public void onPaint(Graphics g){
			
			int xpDif = client.getSkills().getExperience(Skill.CRAFTING) - startXp;
			
			g.drawImage(paint, 0,0, null);
		g.drawString(" " + produce, 70, 42);
		//g.drawString(":: Material: " + material, 325, 62);
		//g.drawString(":: Walkiing To Bank: " + walkingTobank, 325, 112);
		//g.drawString(":: hasNeeded: " + hasNeeded, 325, 162);
		//g.drawString(":: CraftTimer: " + craftTimer, 325, 212);
		g.drawString(" " + runTime(startTime), 70, 56);
		g.drawString(" " + xpDif, 80, 72);
		g.drawString(" " + perHour(xpDif), 60, 87);
		//g.drawString(":: MOUSE X: " + client.getMousePosition().getX(), 325, 412);
		//g.drawString(":: MOUSE Y: " + client.getMousePosition().getY(), 325, 462);
		
		



		
		}
		class MainGUI extends JFrame {
			 Image basicGUI = getImage("http://img6.imageshack.us/img6/5973/l2jp.png");
			    Image jewelery = getImage("http://imageshack.us/a/img138/6130/orna.png");
			    Image armour = getImage("http://imageshack.us/a/img191/2606/vqjw.png");
			    Image gems = getImage("http://img9.imageshack.us/img9/5303/z6v6.png");
			    
			public MainGUI() {
				initComponents();
			}

			private void button1ActionPerformed(ActionEvent e) {
				this.setVisible(false);
				g2.setVisible(true);
				
			}

			private void button2ActionPerformed(ActionEvent e) {
				this.setVisible(false);
				g3.setVisible(true);
			}

			private void button3ActionPerformed(ActionEvent e) {
				this.setVisible(false);
				g4.setVisible(true);
			}

			private void initComponents() {
				// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
				// Generated using JFormDesigner Evaluation license - arogers josh
				label1 = new JLabel();
				button1 = new JButton();
				button2 = new JButton();
				button3 = new JButton();
				label2 = new JLabel();

				//======== this ========
				setResizable(false);
				Container contentPane = getContentPane();
				contentPane.setLayout(null);

				//---- label1 ----
				label1.setText("Murded AIO Crafter");
				label1.setHorizontalAlignment(SwingConstants.CENTER);
				label1.setFont(new Font("Arial Black", Font.PLAIN, 22));
				label1.setForeground(new Color(255, 187, 38));
				contentPane.add(label1);
				label1.setBounds(165, -5, 310, label1.getPreferredSize().height);

				//---- button1 ----
				button1.setIcon(new ImageIcon(armour));
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
					}
				});
				contentPane.add(button1);
				button1.setBounds(40, 135, 160, 130);

				//---- button2 ----
				button2.setIcon(new ImageIcon(gems));
				button2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button2ActionPerformed(e);
					}
				});
				contentPane.add(button2);
				button2.setBounds(235, 135, 160, 130);

				//---- button3 ----
				button3.setIcon(new ImageIcon(jewelery));
				button3.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button3ActionPerformed(e);
					}
				});
				contentPane.add(button3);
				button3.setBounds(430, 135, 160, 130);

				//---- label2 ----
				label2.setIcon(new ImageIcon(basicGUI));
				contentPane.add(label2);
				label2.setBounds(0, -10, 625, 415);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < contentPane.getComponentCount(); i++) {
						Rectangle bounds = contentPane.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = contentPane.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					contentPane.setMinimumSize(preferredSize);
					contentPane.setPreferredSize(preferredSize);
				}
				pack();
				setLocationRelativeTo(getOwner());
				// JFormDesigner - End of component initialization  //GEN-END:initComponents
			}

			// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
			// Generated using JFormDesigner Evaluation license - arogers josh
			private JLabel label1;
			private JButton button1;
			private JButton button2;
			private JButton button3;
			private JLabel label2;
			// JFormDesigner - End of variables declaration  //GEN-END:variables
		}



		class ARMOURGUI extends JFrame {
			public ARMOURGUI() {
				initComponents();
			}

			private void button1ActionPerformed(ActionEvent e) {
				
				if(comboBox1.getSelectedIndex() == 0) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather gloves";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 127;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 1) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather boots";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 129;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 2) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather cowl";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 137;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 3) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather vambraces";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 131;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 4) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather body";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 125;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 5) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Leather chaps";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 133;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 6) {
					material = HARD_LEATHER;
					produce = "Hardleather body";
					ammountToWithdraw = 26;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 7) {
					method = "ARMOUR";
					material = LEATHER;
					produce = "Coif";
					ammountToWithdraw = 26;
					interfaceID = 154;
					childID = 135;
					state = State.setup;
					waitGui = false;
				}
				
				
				
				
				if(comboBox1.getSelectedIndex() == 8) {
					method = "ARMOUR2";
					material = SNAKESKIN;
					produce = "Snakeskin boots";
					ammountToWithdraw = 26;
					interfaceID = 306;
					childID = 26;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 9) {
					method = "ARMOUR2";
					material = SNAKESKIN;
					produce = "Snakeskin bandana";
					ammountToWithdraw = 26;
					interfaceID = 306;
					childID = 22;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 10) {
					method = "ARMOUR2";
					material = SNAKESKIN;
					produce = "Snakeskin vambraces";
					ammountToWithdraw = 26;
					interfaceID = 306;
					childID = 18;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 11) {
					method = "ARMOUR2";
					material = SNAKESKIN;
					produce = "Snakeskin chaps";
					ammountToWithdraw = 26;
					interfaceID = 306;
					childID = 14;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 12) {
					method = "ARMOUR2";
					material = SNAKESKIN;
					produce = "Snakeskin body";
					ammountToWithdraw = 26;
					interfaceID = 306;
					childID = 10;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 13) {
					method = "ARMOUR2";
					material = GREENDHIDE;
					produce = "Green d'hide vambraces";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 12;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 14) {
					method = "ARMOUR2";
					material = GREENDHIDE;
					produce = "Green d'hide chaps";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 16;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 15) {
					method = "ARMOUR2";
					material = GREENDHIDE;
					produce = "Green d'hide body";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 8;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 16) {
					method = "ARMOUR2";
					material = BLUEDHIDE;
					produce = "Blue d'hide vambraces";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 16; // 16
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 17) {
					method = "ARMOUR2";
					material = BLUEDHIDE;
					produce = "Blue d'hide chaps";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 12;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 18) {
					method = "ARMOUR2";
					material = BLUEDHIDE;
					produce = "Blue d'hide body";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 8;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 19) {
					method = "ARMOUR2";
					material = REDDHIDE;
					produce = "Red d'hide vambraces";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 16;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 20) {
					method = "ARMOUR2";
					material = REDDHIDE;
					produce = "Red d'hide chaps";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 12;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 21) {
					method = "ARMOUR2";
					material = REDDHIDE;
					produce = "Red d'hide body";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 8;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 22) {
					method = "ARMOUR2";
					material = BLACKDHIDE;
					produce = "Black d'hide vambraces";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 16;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 23) {
					method = "ARMOUR2";
					material = BLACKDHIDE;
					produce = "Black d'hide chaps";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 12;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 24) {
					method = "ARMOUR2";
					material = BLACKDHIDE;
					produce = "Black d'hide body";
					ammountToWithdraw = 26;
					interfaceID = 304;
					childID = 8;
					state = State.setup;
					waitGui = false;

				}
				
				int currsel = comboBox1.getSelectedIndex();
				
				
				log("Currently Selected is :  " + currsel);
				
				
				
			}

			private void initComponents() {
				// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
				// Generated using JFormDesigner Evaluation license - arogers josh
				label1 = new JLabel();
				button1 = new JButton();
				comboBox1 = new JComboBox<>();
				label2 = new JLabel();

				//======== this ========
				setResizable(false);
				Container contentPane = getContentPane();
				contentPane.setLayout(null);

				//---- label1 ----
				label1.setText("Create Armour");
				label1.setHorizontalAlignment(SwingConstants.CENTER);
				label1.setFont(new Font("Arial Black", Font.PLAIN, 22));
				label1.setForeground(new Color(255, 187, 38));
				contentPane.add(label1);
				label1.setBounds(165, -5, 310, label1.getPreferredSize().height);

				//---- button1 ----
				button1.setText("Start");
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
						button1ActionPerformed(e);
					}
				});
				contentPane.add(button1);
				button1.setBounds(190, 295, 248, 68);

				//---- comboBox1 ----
				comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
					"Leather Gloves",
					"Leather Boots",
					"Leather Cowl",
					"Leather vambraces",
					"Leather Body",
					"Leather Chaps",
					"Hard Leather Body",
					"Coif",
					"Snakeskin boots",
					"Snakeskin bandana",
					"Snakeskin vambraces",
					"Snakeskin chaps",
					"Skaeskin body",
					"Green d'hide Vambraces",
					"Green d'hide Chaps",
					"Green d'hide Body",
					"Blue d'hide Vambraces",
					"Blue d'hide Chaps",
					"Blue d'hide Body",
					"Red d'hide Vambraces",
					"Red d'hide Chaps",
					"Red d'hide Body",
					"Black d'hide Vambraces",
					"Black d'hide Chaps",
					"Black d'hide Body"
				}));
				contentPane.add(comboBox1);
				comboBox1.setBounds(205, 125, 225, 35);

				//---- label2 ----
				label2.setIcon(new ImageIcon("C:\\Users\\Andrew\\Desktop\\basicGUI.png"));
				contentPane.add(label2);
				label2.setBounds(0, -10, 625, 415);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < contentPane.getComponentCount(); i++) {
						Rectangle bounds = contentPane.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = contentPane.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					contentPane.setMinimumSize(preferredSize);
					contentPane.setPreferredSize(preferredSize);
				}
				pack();
				setLocationRelativeTo(getOwner());
				// JFormDesigner - End of component initialization  //GEN-END:initComponents
			}

			// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
			// Generated using JFormDesigner Evaluation license - arogers josh
			private JLabel label1;
			private JButton button1;
			private JComboBox<String> comboBox1;
			private JLabel label2;
			// JFormDesigner - End of variables declaration  //GEN-END:variables
		}
		
		class GEMSGUI extends JFrame {
			public GEMSGUI() {
				initComponents();
			}

			private void button1ActionPerformed(ActionEvent e) {
				
				if(comboBox1.getSelectedIndex() == 0) {
					method = "GEMS";
					material = U_OPAL;
					produce = "Opal";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 1) {
					method = "GEMS";
					material = U_JADE;
					produce = "Jade";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 2) {
					method = "GEMS";
					material = U_TOPAZ;
					produce = "Red topaz";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				
				
				
				if(comboBox1.getSelectedIndex() == 3) {
					method = "GEMS";
					material = U_SAPPHIRE;
					produce = "Sapphire";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 4) {
					method = "GEMS";
					material = U_EMERALD;
					produce = "Emerald";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 5) {
					method = "GEMS";
					material = U_RUBY;
					produce = "Ruby";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 6) {
					method = "GEMS";
					material = U_DIAMOND;
					produce = "Diamond";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 7) {
					method = "GEMS";
					material = U_DRAGONSTONE;
					produce = "Sapphire";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 8) {
					method = "GEMS";
					material = U_ONYX;
					produce = "Onyx";
					ammountToWithdraw = 27;
					interfaceID = 309;
					childID = 6;
					state = State.setup;
					waitGui = false;

				}
			}

			private void initComponents() {
				// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
				// Generated using JFormDesigner Evaluation license - arogers josh
				label1 = new JLabel();
				button1 = new JButton();
				comboBox1 = new JComboBox<>();
				label2 = new JLabel();

				//======== this ========
				setResizable(false);
				Container contentPane = getContentPane();
				contentPane.setLayout(null);

				//---- label1 ----
				label1.setText("Cut Gems");
				label1.setHorizontalAlignment(SwingConstants.CENTER);
				label1.setFont(new Font("Arial Black", Font.PLAIN, 22));
				label1.setForeground(new Color(255, 187, 38));
				contentPane.add(label1);
				label1.setBounds(165, -5, 310, label1.getPreferredSize().height);

				//---- button1 ----
				button1.setText("Start");
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
						button1ActionPerformed(e);
					}
				});
				contentPane.add(button1);
				button1.setBounds(190, 295, 248, 68);

				//---- comboBox1 ----
				comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
					"Cut Opal",
					"Cut Jade",
					"Cut Red Topaz",
					"Cut Sapphire",
					"Cut Emerald",
					"Cut Ruby",
					"Cut Diamond",
					"Cut Dragonstone",
					"Cut Onyx"
				}));
				contentPane.add(comboBox1);
				comboBox1.setBounds(205, 125, 225, 35);

				//---- label2 ----
				label2.setIcon(new ImageIcon("C:\\Users\\Andrew\\Desktop\\basicGUI.png"));
				contentPane.add(label2);
				label2.setBounds(0, -10, 625, 415);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < contentPane.getComponentCount(); i++) {
						Rectangle bounds = contentPane.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = contentPane.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					contentPane.setMinimumSize(preferredSize);
					contentPane.setPreferredSize(preferredSize);
				}
				pack();
				setLocationRelativeTo(getOwner());
				// JFormDesigner - End of component initialization  //GEN-END:initComponents
			}

			// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
			// Generated using JFormDesigner Evaluation license - arogers josh
			private JLabel label1;
			private JButton button1;
			private JComboBox<String> comboBox1;
			private JLabel label2;
			// JFormDesigner - End of variables declaration  //GEN-END:variables
		}
		
		class JEWELERY extends JFrame {
			public JEWELERY() {
				initComponents();
			}

			private void button1ActionPerformed(ActionEvent e) {
				
				if(comboBox1.getSelectedIndex() == 0) {
					method = "JEWELERY";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					produce = "Gold ring";
					ammountToWithdraw = 27;
					interfaceID = 446;
					childID = 68;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 1) {
					method = "JEWELERY";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					produce = "Gold necklacae";
					ammountToWithdraw = 27;
					interfaceID = 446;
					childID = 115;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 2) {
					method = "JEWELERY";
					material = GOLDBAR;
					MOULD = MOULD_BRACELET;
					produce = "Gold bracelet";
					ammountToWithdraw = 27;
					interfaceID = 446;
					childID = 212;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 3) {
					method = "JEWELERY";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					produce = "Gold amulet";
					ammountToWithdraw = 27;
					interfaceID = 446;
					childID = 162;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 4) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					gemID = C_SAPPHIRE;
					produce = "Sapphire ring";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 74;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 5) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					gemID = C_SAPPHIRE;
					produce = "Sapphire necklace";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 121;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 6) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_BRACELET;
					gemID = C_SAPPHIRE;
					produce = "Sapphire bracelet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 218;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 7) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					gemID = C_SAPPHIRE;
					produce = "Sapphire amulet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 168;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 8) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					gemID = C_EMERALD;
					produce = "Emerald ring";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 83;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 9) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					gemID = C_EMERALD;
					produce = "Emerald necklace";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 127;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 10) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_BRACELET;
					gemID = C_EMERALD;
					produce = "Emerald bracelet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 218;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 11) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					gemID = C_EMERALD;
					produce = "Emerald amulet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 174;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 12) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					gemID = C_RUBY;
					produce = "Ruby ring";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 86;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 13) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					gemID = C_RUBY;
					produce = "Ruby necklace";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 133;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 14) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_BRACELET;
					gemID = C_RUBY;
					produce = "Ruby bracelet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 224;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 15) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					gemID = C_RUBY;
					produce = "Ruby amulet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 180;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 16) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					gemID = C_DIAMOND;
					produce = "Diamond ring";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 92;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 17) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					gemID = C_DIAMOND;
					produce = "Diamond necklace";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 139;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 18) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_BRACELET;
					gemID = C_DIAMOND;
					produce = "Diamond bracelet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 236;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 19) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					gemID = C_DIAMOND;
					produce = "Diamond amulet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 92;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 20) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_RING;
					gemID = C_DRAGONSTONE;
					produce = "Dragonstone ring";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 98;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 21) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_NECKLACE;
					gemID = C_DRAGONSTONE;
					produce = "Dragonstone necklace";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 145;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 22) {
					method = "JEWELERY2";
					material = GOLDBAR;
					gemID = C_DRAGONSTONE;
					MOULD = MOULD_BRACELET;
					produce = "Dragonstone bracelet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 242;
					state = State.setup;
					waitGui = false;

				}
				
				if(comboBox1.getSelectedIndex() == 23) {
					method = "JEWELERY2";
					material = GOLDBAR;
					MOULD = MOULD_AMULET;
					gemID = C_DRAGONSTONE;
					produce = "Dragonstone amulet";
					ammountToWithdraw = 13;
					interfaceID = 446;
					childID = 192;
					state = State.setup;
					waitGui = false;

				}
				
				
				
				
				
				
				
				
				
			}

			private void initComponents() {
				// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
				// Generated using JFormDesigner Evaluation license - arogers josh
				label1 = new JLabel();
				button1 = new JButton();
				comboBox1 = new JComboBox<>();
				label2 = new JLabel();

				//======== this ========
				setResizable(false);
				Container contentPane = getContentPane();
				contentPane.setLayout(null);

				//---- label1 ----
				label1.setText("Create Jewelery");
				label1.setHorizontalAlignment(SwingConstants.CENTER);
				label1.setFont(new Font("Arial Black", Font.PLAIN, 22));
				label1.setForeground(new Color(255, 187, 38));
				contentPane.add(label1);
				label1.setBounds(165, -5, 310, label1.getPreferredSize().height);

				//---- button1 ----
				button1.setText("Start");
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
						button1ActionPerformed(e);
					}
				});
				contentPane.add(button1);
				button1.setBounds(190, 295, 248, 68);

				//---- comboBox1 ----
				comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
					"Gold ring",
					"Gold necklace",
					"Gold bracelet",
					"Gold amulet",
					"Saphire ring",
					"Saphire necklace",
					"Saphire bracelet",
					"Saphire amulet",
					"Emerald ring",
					"Emerald necklace",
					"Emerald bracelet",
					"Emerald amulet",
					"Ruby ring",
					"Ruby necklace",
					"Ruby bracelet",
					"Ruby amulet",
					"Diamond ring",
					"Diamond necklace",
					"Diamond bracelet",
					"Diamond amulet",
					"Dragonstone ring",
					"Dragonstone necklace",
					"Dragonstone bracelet",
					"Dragonstone amulet"
				}));
				contentPane.add(comboBox1);
				comboBox1.setBounds(205, 125, 225, 35);

				//---- label2 ----
				label2.setIcon(new ImageIcon("C:\\Users\\Andrew\\Desktop\\basicGUI.png"));
				contentPane.add(label2);
				label2.setBounds(0, -10, 625, 415);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < contentPane.getComponentCount(); i++) {
						Rectangle bounds = contentPane.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = contentPane.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					contentPane.setMinimumSize(preferredSize);
					contentPane.setPreferredSize(preferredSize);
				}
				pack();
				setLocationRelativeTo(getOwner());
				// JFormDesigner - End of component initialization  //GEN-END:initComponents
			}

			// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
			// Generated using JFormDesigner Evaluation license - arogers josh
			private JLabel label1;
			private JButton button1;
			private JComboBox<String> comboBox1;
			private JLabel label2;
			// JFormDesigner - End of variables declaration  //GEN-END:variables
		}



		

	}
		